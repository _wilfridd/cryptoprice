��          �      l      �     �     �  #   �          +     7  $   R  	   w     �     �  
   �     �     �     �     �     �     �     �             b        �     �  &   �     �     �  $   �  #        )     1     D     K     T     a  	   o     y     �     �     �     �     �                  
                                                           	                     About About CryptoPrice Automatic Updates every 60 seconds? Buy me a beer CryptoPrice Do you want to see labels? Do you want to see the price arrows? Donations Issues & Wishes Mail Maintainer My Work Settings Source Source of prices? Support Support my work Version %1. Under License %2 Your currency? hosted at Gitlab Project-Id-Version: cryptoprice.maltekiefer
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-01 12:12+0200
Last-Translator: Malte Kiefer <malte.kiefer@mailgermania.de>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Über Über CryptoPrice Automatisches Update alle 60 Sekunden? Kauf mir ein Bier CryptoPrice Möchten Sie die Beschriftung sehen? Möchten Sie die Preispfeile sehen? Spenden Fehler & Wünsche: E-Mail Betreuer Meine Arbeit Einstellungen Quellcode Preisquelle? Unterstützung Unterstütze meine Arbeit Version %1. Unter Lizenz %2 Deine Währung? gehostet bei Gitlab 