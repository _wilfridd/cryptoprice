/*
    ICONS: https://github.com/atomiclabs/cryptocurrency-icons/
*/

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'cryptoprice.maltekiefer'
    automaticOrientation: true

    //Colors
    property string frontColor:     "#000000"  // or "#000000" format
    property string backColor:      "#CFCFCF"    // or "#000000" format
    property string detailsColor:   "#949494"      // or "#000000" format
    property string accentColor:    "#CFCFCF"    // or "#000000" format

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings
        property bool automaticRefresh: false
        property bool showArrows: true
        property bool showLabels: false
        property int currentIndex: 6
        property int apiIndex: 0
        property string api: "CRYPTOCOMPARE"
        property string userCurrency: "USD"
        property string userCurrencySymbol: "$"
        property string userCurrencySymbolPosition: "left"
        property bool showADA: true
        property bool showBCH: true
        property bool showBTC: true
        property bool showDASH: true
        property bool showDOGE: true
        property bool showEOS: true
        property bool showETC: true
        property bool showETH: true
        property bool showFIL: true
        property bool showLTC: true
        property bool showSLP: true
        property bool showXLM: true
        property bool showXMR: true
        property bool showXRP: true
    }

    Settings {
        id: curSettings
        property string ada: "0"
        property string bch: "0"
        property string btc: "0"
        property string dash: "0"
        property string doge: "0"
        property string eos: "0"
        property string etc: "0"
        property string eth: "0"
        property string fil: "0"
        property string ltc: "0"
        property string slp: "0"
        property string xlm: "0"
        property string xrp: "0"
        property string xmr: "0"
    }

    Timer {
        id: timer
        interval: 60000
        repeat: settings.automaticRefresh
        running: settings.automaticRefresh

        onTriggered:
        {
            getPrice()
        }
    }

    PageStack {
        id: pageStack
        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            anchors.fill: parent
            header: PageHeader {
                id: header
                title: 'CryptoPrice'
                StyleHints {
                    foregroundColor: frontColor
                    backgroundColor: backColor
                    dividerColor: detailsColor
                }
                trailingActionBar {
                    actions: [
                        Action {
                            iconName: "info"
                            text: "infos"

                            onTriggered: pageStack.push(Qt.resolvedUrl("About.qml"))
                        },
                        Action {
                            iconName: "settings"
                            text: "settings"

                            onTriggered: pageStack.push(Qt.resolvedUrl("Settings.qml"))
                        },
                        Action {
                            iconName: "reload"
                            text: "refresh"

                            onTriggered: getPrice()
                        }
                    ]
                }
            }
            Flickable {
                anchors.fill: parent
                anchors.top: parent.bottom
                anchors.topMargin: units.gu(6.3)
                contentHeight: main.height + units.gu(10) + main.anchors.topMargin
                id: flick
                GridLayout {
                    id: main
                    rowSpacing: units.gu(1.5)
                    columnSpacing: units.gu(1.5)
                    columns: 4
                    anchors.horizontalCenter: parent.horizontalCenter

                    /*
                    * ADA
                    */

                    Image {
                        source: "assets/ada.png"
                        visible: settings.showADA
                        Image {
                            id: imgada
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: settings.showLabels ? "ADA" : ""
                        color: "#000000"
                        visible: settings.showADA
                    }
                    Label {
                        id: lb_ada
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.ada
                        visible: settings.showADA
                        color: "#000000"
                    }
                    Label {
                        id: lb_adaarrow
                        text: ""
                        visible: settings.showADA
                    }

                    /*
                    * BTC
                    */

                    Image {
                        source: "assets/btc.png"
                        visible: settings.showBTC
                        Image {
                            id: imgbtc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "BTC" : ""
                      visible: settings.showBTC
                      color: "#000000"
                    }
                    Label {
                        id: lb_btc
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.btc
                        visible: settings.showBTC
                        color: "#000000"
                    }
                    Label {
                        id: lb_btcarrow
                        text: ""
                        visible: settings.showBTC
                        horizontalAlignment: Text.AlignRight
                    }

                    /*
                    * BCH
                    */

                    Image {
                        source: settings.showBCH ? "assets/bch.png" : ''
                        visible: settings.showBCH
                        Image {
                            id: imgbch
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "BCH" : ""
                      visible: settings.showBCH
                      color: "#000000"
                    }
                    Label {
                        id: lb_bch
                         Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.bch
                        visible: settings.showBCH
                        color: "#000000"
                    }
                    Label {
                        id: lb_bcharrow
                        text: ""
                        visible: settings.showBCH
                    }

                    /*
                    * Dash
                    */

                    Image {
                        source: "assets/dash.png"
                        visible: settings.showDASH
                        Image {
                            id: imgdash
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "Dash" : ""
                      visible: settings.showDASH
                      color: "#000000"
                    }
                    Label {
                        id: lb_dash
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.dash
                        visible: settings.showDASH
                        color: "#000000"
                    }
                    Label {
                        id: lb_dasharrow
                        visible: settings.showDASH
                        text: ""
                    }
                    /*
                    * Doge
                    */

                    Image {
                        source: "assets/doge.png"
                        visible: settings.showDOGE
                        Image {
                            id: imgdoge
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "Doge" : ""
                      visible: settings.showDOGE
                      color: "#000000"
                    }
                    Label {
                        id: lb_doge
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.doge
                        visible: settings.showDOGE
                        color: "#000000"
                    }
                    Label {
                        id: lb_dogearrow
                        visible: settings.showDOGE
                        text: ""
                    }

                    /*
                    * EOS
                    */

                    Image {
                        source: "assets/eos.png"
                        visible: settings.showEOS
                        Image {
                            id: imgeos
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: settings.showLabels ? "EOS" : ""
                        visible: settings.showEOS
                        color: "#000000"
                    }
                    Label {
                        id: lb_eos
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.eos
                        visible: settings.showEOS
                        color: "#000000"
                    }
                    Label {
                        id: lb_eosarrow
                        visible: settings.showEOS
                        text: ""
                    }

                    /*
                    * ETH
                    */

                    Image {
                        source: "assets/eth.png"
                        visible: settings.showETH
                        Image {
                            id: imgeth
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "ETH" : ""
                      visible: settings.showETH
                      color: "#000000"
                    }
                    Label {
                        id: lb_eth
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.eth
                        visible: settings.showETH
                        color: "#000000"
                    }
                    Label {
                        id: lb_etharrow
                        visible: settings.showETH
                        text: ""
                    }

                    /*
                    * ETC
                    */

                    Image {
                        source: "assets/etc.png"
                        visible: settings.showETC
                        Image {
                            id: imgetc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: settings.showLabels ? "ETC" : ""
                        visible: settings.showETC
                        color: "#000000"
                    }
                    Label {
                        id: lb_etc
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.etc
                        visible: settings.showETC
                        color: "#000000"
                    }
                    Label {
                        id: lb_etcarrow
                        visible: settings.showETC
                        text: ""
                    }

                    /*
                    * FIL
                    */

                    Image {
                        source: "assets/fil.png"
                        visible: settings.showFIL
                        Image {
                            id: imgfil
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: settings.showLabels ? "FIL" : ""
                        visible: settings.showFIL
                        color: "#000000"
                    }
                    Label {
                        id: lb_fil
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.fil
                        visible: settings.showFIL
                        color: "#000000"
                    }
                    Label {
                        id: lb_filarrow
                        visible: settings.showFIL
                        text: ""
                    }

                    /*
                    * LTC
                    */

                    Image {
                        source: "assets/ltc.png"
                        visible: settings.showLTC
                        Image {
                            id: imgltc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "LTC" : ""
                      visible: settings.showLTC
                      color: "#000000"
                    }
                    Label {
                        id: lb_ltc
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.ltc
                        visible: settings.showLTC
                        color: "#000000"
                    }
                    Label {
                        id: lb_ltcarrow
                        visible: settings.showLTC
                        text: ""
                    }

                    /*
                    * SLP
                    */

                    Image {
                        source: "assets/slp.png"
                        visible: settings.showSLP
                        Image {
                            id: imgslp
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "SLP" : ""
                      color: "#000000"
                      visible: settings.showSLP
                    }
                    Label {
                        id: lb_slp
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.slp
                        color: "#000000"
                        visible: settings.showSLP
                    }
                    Label {
                        id: lb_slparrow
                        text: ""
                        visible: settings.showSLP
                    }


                    /*
                    * XLM
                    */

                    Image {
                        source: "assets/xlm.png"
                        visible: settings.showXLM
                        Image {
                            id: imgxlm
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "XLM" : ""
                      visible: settings.showXLM
                      color: "#000000"
                    }
                    Label {
                        id: lb_xlm
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.xlm
                        visible: settings.showXLM
                        color: "#000000"
                    }
                    Label {
                        id: lb_xlmarrow
                        visible: settings.showXLM
                        text: ""
                    }

                    /*
                    * XMR
                    */

                    Image {
                        source: "assets/xmr.png"
                        visible: settings.showXMR
                        Image {
                            id: imgxmr
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "XMR" : ""
                      visible: settings.showXMR
                      color: "#000000"
                    }
                    Label {
                        id: lb_xmr
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.xmr
                        visible: settings.showXMR
                        color: "#000000"
                    }
                    Label {
                        id: lb_xmrarrow
                        visible: settings.showXMR
                        text: ""
                    }

                    /*
                    * XRP
                    */

                    Image {
                        source: "assets/xrp.png"
                        visible: settings.showXRP
                        Image {
                            id: imgxrp
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }

                    Label {
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: settings.showLabels ? "XRP" : ""
                        visible: settings.showXRP
                        color: "#000000"
                    }

                    Label {
                        id: lb_xrp
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.xrp
                        visible: settings.showXRP
                        color: "#000000"
                    }

                    Label {
                        id: lb_xrparrow
                        visible: settings.showXRP
                        text: ""
                    }

                }
            }

            Component.onCompleted: {
                getPrice()
            }
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('cryptoprice', function() {
                console.log("-------------- python loaded");
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    function getPrice() {

      const crypto = ["ADA", "BCH", "BTC", "DASH", "DOGE", "EOS", "ETC", "ETH", "FIL", "LTC", "SLP", "XLM", "XMR", "XRP"];

      for (let curr of crypto) {
        python.call('cryptoprice.get_latest_price', [settings.api, curr, settings.userCurrency], function(returnValue) {
          !returnValue ? console.log("-------------- Error: " + settings.userCurrency + " => " + curr) : console.log("-------------- Done: " + settings.userCurrency + " => " + curr)
          switch(curr){
            case 'ADA':
                settings.showADA = !returnValue ? false: true
                lb_ada.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_adaarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.ada == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.ada ? "\u2B07" : "\u2B06"
                lb_ada.color = !returnValue ? '' : curSettings.ada == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.ada ? "#e60000" : "#009933"
                curSettings.ada = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'BCH':
                settings.showBCH = !returnValue ? false : true
                lb_bch.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_bcharrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.bch == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.bch ? "\u2B07" : "\u2B06"
                lb_bch.color = !returnValue ? '' : curSettings.bch == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.bch ? "#e60000" : "#009933"
                curSettings.bch = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'BTC':
                settings.showBTC = !returnValue ? false : true
                lb_btc.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_btcarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.btc == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.btc ? "\u2B07" : "\u2B06"
                lb_btc.color = !returnValue ? '' : curSettings.btc == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.btc ? "#e60000" : "#009933"
                curSettings.btc = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'DASH':
                settings.showDASH = !returnValue ? false : true
                lb_dash.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_dasharrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.dash == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.dash ? "\u2B07" : "\u2B06"
                lb_dash.color = !returnValue ? '' : curSettings.dash == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.dash ? "#e60000" : "#009933"
                curSettings.dash = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'DOGE':
                settings.showDOGE = !returnValue ? false : true
                lb_doge.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_dogearrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.doge == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.doge ? "\u2B07" : "\u2B06"
                lb_doge.color = !returnValue ? '' : curSettings.doge == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.doge ? "#e60000" : "#009933"
                curSettings.doge = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'EOS':
                settings.showEOS = !returnValue ? false : true
                lb_eos.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_eosarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.eos == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.eos ? "\u2B07" : "\u2B06"
                lb_eos.color = !returnValue ? '' : curSettings.eos == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.eos ? "#e60000" : "#009933"
                curSettings.eos = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'ETC':
                settings.showETC = !returnValue ? false : true
                lb_etc.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_etcarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.etc == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.etc ? "\u2B07" : "\u2B06"
                lb_etc.color = !returnValue ? '' : curSettings.etc == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.etc ? "#e60000" : "#009933"
                curSettings.etc = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'ETH':
                settings.showETH = !returnValue ? false : true
                lb_eth.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_etharrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.eth == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.eth ? "\u2B07" : "\u2B06"
                lb_eth.color = !returnValue ? '' : curSettings.eth == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.eth ? "#e60000" : "#009933"
                curSettings.eth = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'FIL':
                settings.showFIL = !returnValue ? false : true
                lb_fil.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_filarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.fil == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.fil ? "\u2B07" : "\u2B06"
                lb_fil.color = !returnValue ? '' : curSettings.fil == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.fil ? "#e60000" : "#009933"
                curSettings.fil = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'LTC':
                settings.showLTC = !returnValue ? false : true
                lb_ltc.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_ltcarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.ltc == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.ltc ? "\u2B07" : "\u2B06"
                lb_ltc.color = !returnValue ? '' : curSettings.ltc == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.ltc ? "#e60000" : "#009933"
                curSettings.ltc = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'SLP':
                settings.showSLP = !returnValue ? false : true
                lb_slp.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_slparrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.slp == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.slp ? "\u2B07" : "\u2B06"
                lb_slp.color = !returnValue ? '' : curSettings.slp == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.slp ? "#e60000" : "#009933"
                curSettings.slp = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'XLM':
                settings.showXLM = !returnValue ? false : true
                lb_xlm.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_xlmarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.xlm == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.xlm ? "\u2B07" : "\u2B06"
                lb_xlm.color = !returnValue ? '' : curSettings.xlm == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.xlm ? "#e60000" : "#009933"
                curSettings.xlm = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'XMR':
                settings.showXMR = !returnValue ? false : true
                lb_xmr.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_xmrarrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.xmr == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.xmr ? "\u2B07" : "\u2B06"
                lb_xmr.color = !returnValue ? '' : curSettings.xmr == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.xmr ? "#e60000" : "#009933"
                curSettings.xmr = !returnValue ? false : (returnValue).toFixed(2)
              break;
            case 'XRP':
                settings.showXRP = !returnValue ? false : true
                lb_xrp.text = !returnValue ? '' : settings.userCurrencySymbolPosition == "left" ? "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>" : "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
                lb_xrparrow.text = !returnValue ? '' : !settings.showArrows ? "" : curSettings.xrp == (returnValue).toFixed(2) ? "\u2B0C" : returnValue < curSettings.xrp ? "\u2B07" : "\u2B06"
                lb_xrp.color = !returnValue ? '' : curSettings.xrp == (returnValue).toFixed(2) ? "#000000" : returnValue < curSettings.xrp ? "#e60000" : "#009933"
                curSettings.xrp = !returnValue ? false : (returnValue).toFixed(2)
              break;
            }
          }
        )
      }
        done()
    }

    function done() {
        console.log("-------------- updated all cryptoprices")
    }
}
